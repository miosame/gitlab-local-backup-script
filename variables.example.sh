#!/usr/bin/env bash

# gitlab username
USERNAME=""

# generate one here: https://gitlab.com/-/profile/personal_access_tokens
# with read_api permission
PRIVATE_TOKEN=""

# mail settings, see: https://github.com/caronc/apprise/wiki/Notify_email
MAIL_USERNAME=""
MAIL_DOMAIN=""

# generate an app password and allow smtp on your mail provider
MAIL_APP_PASSWORD=""

# the email to send the notification to
MAIL_TO=""
