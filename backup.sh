#!/usr/bin/env bash

if ! [ -x "$(command -v jq)" ]; then
  echo 'Error: jq is not installed.' >&2
  exit 1
fi

if ! [ -x "$(command -v apprise)" ]; then
  echo 'Error: apprise is not installed.' >&2
  exit 1
fi

# change to the directory the script is in, so ./repos is created there
# instead of wherever the cronjob is running it from

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR

source "variables.sh"

page=1
while true
do
    REPOS=($(curl -s "https://gitlab.com/api/v4/projects?private_token=${PRIVATE_TOKEN}&owned=true&per_page=100&page=${page}" | jq -r '.[].path_with_namespace | @sh'))
    if (( ${#REPOS[@]} )); then
        for REPO in "${REPOS[@]}"
        do 
            REPO_FOLDER="./repos/${REPO//\'}"
            mkdir -p $REPO_FOLDER

            if git ls-remote $REPO_FOLDER -q 2>&1; then
                echo "[pulling] ${REPO//\'}"
                PULLED=$(git -C $REPO_FOLDER pull origin master)
                if [[ $PULLED == *"Already up to date"* ]]; then
                    echo "Already up to date.. nothing to do"
                else
                    apprise -t "[GITLAB-BACKUP] Updated repository" -b "Found update on repository: ${REPO//\'}" "mailto://${MAIL_USERNAME}:${MAIL_APP_PASSWORD}@${MAIL_DOMAIN}/?to=${MAIL_TO}"
                fi
            else
                echo "[cloning] ${REPO//\'}"
                git clone "https://${USERNAME}:${PRIVATE_TOKEN}@gitlab.com/${REPO//\'}.git" $REPO_FOLDER
                apprise -t "[GITLAB-BACKUP] New repository" -b "Found new repository: ${REPO//\'}" "mailto://${MAIL_USERNAME}:${MAIL_APP_PASSWORD}@${MAIL_DOMAIN}/?to=${MAIL_TO}"
            fi
        done
    else
        break
    fi

    page=$(( $page + 1 ))
done
