# gitlab local backup script

this script supports pagination and automatically pulls instead of clones if the repository exists already, it could also be run with parameters, to backup multiple accounts (`$USERNAME` would then just take in e.g. `$1` instead of a string, but that's up to you).

the repositories it clones are stored wherever the script is, e.g. `./repos/username/nice-repository-name`

dependencies:
- jq
- [apprise](https://github.com/caronc/apprise) (for email notifications)
- bash (I assume half of this is bashisms, you're welcome to try it on esoteric shells)

copy `variables.example.sh` to `variables.sh` and change the values, the comments inside explain it all.

now just make `backup.sh` executable and throw it into a cronjob.
